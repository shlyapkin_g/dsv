/**
 * Evaluate hash of the passed number.
 * @param value - Argument to use.
 * @returns {number|*} - Hash of the passed number.
 */
const hsh = value => {
  value ^= value >> 16;
  value *= 0x85ebca6b;
  value ^= value >> 13;
  value *= 0xc2b2ae35;
  value ^= value >> 16;

  return value;
};

/**
 * Evaluate hash of the passed value.
 * @param {*} value - Argument to evaluate.
 * @returns {number} - Evaluated hash.
 */
const hash = value => {
  let tmp = 0;
  if (typeof value === 'string') {
    for (let i = 0; i < value.length; i++) tmp += value[i].charCodeAt(0);
    tmp = hsh(tmp);
  } else {
    tmp = hsh(tmp);
  }

  return tmp;
};

function Stack(value) {
  this.value = value;
  this.next = null;

  this.push = function (value) {
    if (!isUndefined(this.value)) {
      this.next !== null ? this.next.push(value) : this.next = new Stack(value);
    } else {
      this.value = value;
    }
  };

  this.pop = function () {
    if (this.next !== null) {
      this.next.next !== null ? this.next.pop() : this.next = null;
    } else {
      this.value = undefined;
    }
  };

  this.read = () => this.next !== null ? this.read.call(this.next) : this.value;
}

function Queue(value) {
  this.value = value;
  this.next = null;

  this.read = () => this.value;

  this.push = function (value) {
    if (!isUndefined(this.value)) {
      this.next !== null ? this.next.push(value) : this.next = new Queue(value);
    } else {
      this.value = value;
    }
  };

  this.pop = function () {
    if (this.next !== null) {
      this.value = this.next.value;
      this.next = this.next.next;
    } else {
      this.value = null;
    }
  };
}

function BinaryTree(value) {
  this.value = value;
  this.leftNode = this.rightNode = null;

  this.addNode = function (value) {
    if (isUndefined(this.value)) {
      this.value = value;
      this.leftNode = this.rightNode = null;
    }
    else {
      const dir = this.value > value ? 'leftNode' : 'rightNode';
      this[dir] !== null ? this[dir].addNode(value) : this[dir] = new BinaryTree(value);
    }
  };

  this.removeNode = function (value) {
    if (!isUndefined(this.value)) {
      let root = this;
      let parent;
      // Searching current value in tree and storing parent element.
      while (root.value !== value || !isUndefined(root.value)) {
        if (value === root.value) {
          if (isUndefined(parent)) {
            parent = root;
          }
        } else {
          const dir = value < root.value ? 'leftNode' : 'rightNode';
          if (root[dir] === null) {
            parent = null;
          } else {
            parent = root;
            root = root[dir];
          }
        }
      }
      // If we found element then parent is not null.
      if (parent !== null) {
        // If there are two childs.
        if (root.leftNode !== null && root.rightNode !== null) {
          if (root.rightNode.leftNode !== null) {
            var min = root.rightNode.getMin();
          } else {
            var max = root.leftNode.getMax();
          }
          var nodeToDeleteValue = isUndefined(min) ? max : min;
          this.removeNode(nodeToDeleteValue);
          root.value = nodeToDeleteValue;
        } else if (root.leftNode !== null && root.rightNode === null) {
          root.value = root.leftNode.value;
          root.leftNode = null;
        } else if (root.rightNode !== null && root.leftNode === null) {
          root.value = root.rightNode.value;
          root.rightNode = null;
        } else if (root.leftNode === null && root.rightNode === null) {
          if (parent.leftNode === root) {
            parent.leftNode = null;
          } else if (parent.rightNode === root) {
            parent.rightNode = null;
          }
        }
      }
    }
  };

  this.find = function (value) {
    if (!isUndefined(this.value)) {
      if (value === this.value) {
        return true;
      } else {
        const dir = value < this.value ? 'leftNode' : 'rightNode';
        return this[dir] !== null ? this[dir].find(value) : false;
      }
    }
  };
}

function Tree() {}

Tree.prototype.getMin = function () {
  let min;
  if (this.leftNode !== null) {
    return this.leftNode.getMin();
  } else {
    min = this.value;
  }

  return min;
};

Tree.prototype.getMax = function () {
  let max;
  if (this.rightNode !== null) {
    return this.rightNode.getMax();
  } else {
    max = this.value;
  }

  return max;
};

Tree.prototype.smallLeftToRightRotate = function () {
  const a = this;
  const b = this.rightNode;
  const l = this.leftNode;
  const c = this.rightNode.leftNode;
  const r = this.rightNode.rightNode;
  const structure = new BinaryTree(b.value);
  structure.rightNode = r;
  structure.leftNode = new BinaryTree(a.value);
  structure.leftNode.leftNode = l;
  structure.leftNode.rightNode = c;
  this.value = structure.value;
  this.leftNode = structure.leftNode;
  this.rightNode = structure.rightNode;
};

Tree.prototype.smallRightToLeftRotate = function () {
  const b = this;
  const a = this.leftNode;
  const l = this.leftNode.leftNode;
  const c = this.leftNode.rightNode;
  const r = this.rightNode;
  const structure = new BinaryTree(a.value);
  structure.rightNode = new BinaryTree(b.value);
  structure.leftNode = l;
  structure.rightNode.leftNode = c;
  structure.rightNode.rightNode = r;
  this.value = structure.value;
  this.leftNode = structure.leftNode;
  this.rightNode = structure.rightNode;
};

Tree.prototype.bigLeftToRightRotate = function () {
  const a = this;
  const l = this.leftNode;
  const b = this.rightNode;
  const r = this.rightNode.rightNode;
  const c = this.rightNode.leftNode;
  const m = this.rightNode.leftNode.leftNode;
  const n = this.rightNode.leftNode.rightNode;
  const structure = new BinaryTree(c.value);
  structure.leftNode = new BinaryTree(a.value);
  structure.rightNode = new BinaryTree(b.value);
  structure.leftNode.leftNode = l;
  structure.leftNode.rightNode = m;
  structure.rightNode.leftNode = n;
  structure.rightNode.rightNode = r;
  this.value = structure.value;
  this.leftNode = structure.leftNode;
  this.rightNode = structure.rightNode;
};

Tree.prototype.bigRightToLeftRotate = function () {
  const a = this;
  const b = this.leftNode;
  const r = this.rightNode;
  const l = this.leftNode.leftNode;
  const c = this.leftNode.rightNode;
  const m = this.leftNode.rightNode.leftNode;
  const n = this.leftNode.rightNode.leftNode;
  const structure = new BinaryTree(c.value);
  structure.leftNode = new BinaryTree(b.value);
  structure.rightNode = new BinaryTree(a.value);
  structure.leftNode.leftNode = l;
  structure.leftNode.rightNode = m;
  structure.rightNode.leftNode = n;
  structure.rightNode.rightNode = r;
  this.value = structure.value;
  this.leftNode = structure.leftNode;
  this.rightNode = structure.rightNode;
};

Tree.prototype.getHeight = function () {
  let height = 0;
  if (!isUndefined(this.value)) {
    height++;
    let left = this.leftNode === null ? 0 : this.leftNode.getHeight();
    let right = this.rightNode === null ? 0 : this.rightNode.getHeight();
    height += Math.max(left, right);
  }

  return height;
};

// Inherit methods and properties of Tree to Binary tree.
BinaryTree.prototype = AVLTree.prototype = Object.create(Tree.prototype);

function HashTableChains(size) {
  this.elements = new Array(size);
  for (let i = 0; i < size; i++) {
    this.elements[i] = [];
  }

  this.getIndexOf = value => hash(value) % size;

  this.addElement = function (value) {
    this.elements[this.getIndexOf(value)].push(value);
  };

  this.removeElement = function (value) {
    const index = this.getIndexOf(value);
    if (this.elements[index] !== []) {
      let elements = this.elements[index];
      elements.forEach((element, elementIndex, src) => {
        if (value === element) {
          src.splice(elementIndex, 1);
          return false;
        }
      });
    }
  };
}

function HashTableOpenAddresses(size) {
  this.elements = new Array(size);
  for (let i = 0; i < size; i++) {
    this.elements[i] = [];
    this.elements[i]['value'] = undefined;
    this.elements[i]['isDeleted'] = false;
  }

  this.getIndexOf = value => hash(value) % size;

  this.addElement = function (value) {
    let index = this.getIndexOf(value);
    if (!isUndefined(this.elements[index]['value'])) {
      const position = index++;
      if (index === this.elements.length) index = 0;
      while (!isUndefined(this.elements[index]['value'])) {
        if (index !== position) {
          index++;
        } else {
          break;
        }
      }
    }
    this.elements[index]['value'] = value;
    this.elements[index]['isDeleted'] = false;
  };

  this.removeElement = function (value) {
    let index = this.getIndexOf(value);
    if (!isUndefined(this.elements[index]['value'])) {
      if (this.elements[index]['value'] !== value) {
        const position = index++;
        if (index === this.elements.length) index = 0;
        while (this.elements[index]['value'] === value && this.elements[index]['isDeleted'] === false) {
          if (index !== position) {
            index++;
          } else {
            break;
          }
        }
      }
      this.elements[index]['isDeleted'] = true;
    }
  };
}

function AVLTree(value) {
  this.value = value;
  this.rightNode = this.leftNode = null;

  // Define constants.
  const ROTATE = {
    SMALL_LEFT_TO_RIGHT: 1,
    SMALL_RIGHT_TO_LEFT: 2,
    BIG_LEFT_TO_RIGHT: 3,
    BIG_RIGHT_TO_LEFT: 4
  };

  this.checkBalance = function () {
    let result = 0;
    // If null create empty tree with 0 height.
    const r = this.rightNode === null ? new AVLTree() : this.rightNode;
    const l = this.leftNode === null ? new AVLTree() : this.leftNode;
    if (r.getHeight() - l.getHeight() > 1) {
      const rl = this.rightNode.leftNode === null ? new AVLTree() : this.rightNode.leftNode;
      const rr = this.rightNode.rightNode === null ? new AVLTree() : this.rightNode.rightNode;
      if (r.getHeight() - l.getHeight() === 2
        && rl.getHeight() <= rr.getHeight()) {
        result = ROTATE.SMALL_LEFT_TO_RIGHT;
      } else if (r.getHeight() - l.getHeight() === 2
        && rl.getHeight() > rr.getHeight()) {
        result = ROTATE.BIG_LEFT_TO_RIGHT;
      }
    }
    else if (l.getHeight() - r.getHeight() > 1) {
      const lr = this.leftNode.rightNode === null ? new AVLTree() : this.leftNode.rightNode;
      const ll = this.leftNode.leftNode === null ? new AVLTree() : this.leftNode.leftNode;
      if (l.getHeight() - r.getHeight() === 2
        && lr.getHeight() <= ll.getHeight()) {
        result = ROTATE.SMALL_RIGHT_TO_LEFT;
      } else if (this.leftNode.getHeight() - this.rightNode.getHeight() === 2
        && lr.getHeight() > ll.getHeight()) {
        result = ROTATE.BIG_RIGHT_TO_LEFT;
      }
    }

    return result;
  };

  this.balance = function (balanceCode) {
    switch (balanceCode) {
      case ROTATE.SMALL_LEFT_TO_RIGHT:
        this.smallLeftToRightRotate();
        break;

      case ROTATE.SMALL_RIGHT_TO_LEFT:
        this.smallRightToLeftRotate();
        break;

      case ROTATE.BIG_LEFT_TO_RIGHT:
        this.bigLeftToRightRotate();
        break;

      case ROTATE.BIG_RIGHT_TO_LEFT:
        this.bigRightToLeftRotate();
        break;

      default:
        return;
    }
  };

  this.addNode = function (value) {
    if (isUndefined(this.value)) {
      this.value = value;
      this.rightNode = this.leftNode = null;
    } else {
      const dir = value < this.value ? 'leftNode' : 'rightNode';
      this[dir] === null ? this[dir] = new AVLTree(value) : this[dir].addNode(value);
      let balanceCode = this.checkBalance();
      if (balanceCode) this.balance(balanceCode);
    }
  };

  this.removeNode = function (value) {
    if (!isUndefined(this.value)) {
      let root = this, parent;
      // Searching current value in tree and storing parent element.
      while (root.value !== value || !isUndefined(root.value)) {
        if (value === root.value) {
          if (isUndefined(parent)) {
            parent = root;
          }
          break;
        } else if(value < root.value) {
          if (root.leftNode === null) {
            parent = null;
            break;
          } else {
            parent = root;
            root = root.leftNode;
          }
        } else {
          if (root.rightNode === null) {
            parent = null;
            break;
          } else {
            parent = root;
            root = root.rightNode;
          }
        }
      }
      // If we found element then parent is not null.
      if (parent !== null) {
        // If there are two childs.
        if (root.leftNode !== null && root.rightNode !== null) {
          let min, max;
          if (root.rightNode.leftNode !== null) {
            min = root.rightNode.getMin();
          } else {
            max = root.leftNode.getMax();
          }
          const nodeToDeleteValue = isUndefined(min) ? max : min;
          this.removeNode(nodeToDeleteValue);
          root.value = nodeToDeleteValue;
          const balanceCode = this.checkBalance();
          if (balanceCode) this.balance(balanceCode);
        } else if (root.leftNode !== null && root.rightNode === null) {
          root.value = root.leftNode.value;
          root.leftNode = null;
          const balanceCode = this.checkBalance();
          if (balanceCode) this.balance(balanceCode);
        } else if (root.rightNode !== null && root.leftNode === null) {
          root.value = root.rightNode.value;
          root.rightNode = null;
          const balanceCode = this.checkBalance();
          if (balanceCode) this.balance(balanceCode);
        } else if (root.leftNode === null && root.rightNode === null) {
          if (parent.leftNode === root) {
            parent.leftNode = null;
          } else if (parent.rightNode === root) {
            parent.rightNode = null;
          }
          const balanceCode = this.checkBalance();
          if (balanceCode) this.balance(balanceCode);
        }
      }
    }
  }
}

function HeapMax() {
  this.elements = [];

  this.addElement = function (value) {
    if (!this.elements.length) {
      this.elements[1] = value;
    } else {
      var j = this.elements.length - 1, currentIndex = j + 1;
      this.elements[currentIndex] = value;
      while (this.elements[currentIndex] > this.elements[j]) {
        var tmp = this.elements[currentIndex];
        this.elements[currentIndex] = this.elements[j];
        this.elements[j] = tmp;
        currentIndex = j;
        j--;
      }
    }
  };

  this.removeElement = function () {
    var len = this.elements.length;
    if (!isUndefined(this.elements[1]) && this.elements[1] !== this.elements[len - 1]) {
      var j = 2, currentIndex = 1;
      this.elements[1] = this.elements[len - 1];
      this.elements[len - 1] = undefined;
      while (this.elements[currentIndex] > this.elements[j]) {
        var tmp = this.elements[j];
        this.elements[j] = this.elements[currentIndex];
        this.elements[currentIndex] = tmp;
        currentIndex = j;
        j++;
      }
      this.elements.splice(len - 1, 1);
    } else if (!isUndefined(this.elements[1]) && this.elements[1] === this.elements[len - 1]) {
      this.elements.splice(len - 1, 1);
    }
  };
}

function Deque (value) {

}