/**
 * @file
 * Provide custom functions.
 */
var containers = [], texts = [], connections = [];
let tree;

/**
 * Define colors which will be used for fill elements.
 */
const COLOR = {
  GREY: '#aaa',
  BLACK: '#000',
  RED: '#f00',
  GREEN: '#0f0',
  YELLOW: '#ff0'
};

Point = function (x, y, text, depth, parent) {
  this.x = x;
  this.y = y;
  this.text = text;
  this.depth = depth;
  this.parent = parent;
};

(function ($) {
  Raphael.el.isVisible = function () {
    return this.node.style.display !== 'none';
  };
  Raphael.fn.connection = function (obj1, obj2, line, bg) {
    if (obj1.line && obj1.from && obj1.to) {
      line = obj1;
      obj1 = line.from;
      obj2 = line.to;
    }
    var bb1 = obj1.getBBox(),
      bb2 = obj2.getBBox(),
      p = [{x: bb1.x + bb1.width / 2, y: bb1.y - 1},
        {x: bb1.x + bb1.width / 2, y: bb1.y + bb1.height + 1},
        {x: bb1.x - 1, y: bb1.y + bb1.height / 2},
        {x: bb1.x + bb1.width + 1, y: bb1.y + bb1.height / 2},
        {x: bb2.x + bb2.width / 2, y: bb2.y - 1},
        {x: bb2.x + bb2.width / 2, y: bb2.y + bb2.height + 1},
        {x: bb2.x - 1, y: bb2.y + bb2.height / 2},
        {x: bb2.x + bb2.width + 1, y: bb2.y + bb2.height / 2}],
      d = {}, dis = [];
    for (var i = 0; i < 4; i++) {
      for (var j = 4; j < 8; j++) {
        var dx = Math.abs(p[i].x - p[j].x),
          dy = Math.abs(p[i].y - p[j].y);
        if ((i == j - 4) || (((i != 3 && j != 6) || p[i].x < p[j].x) && ((i != 2 && j != 7) || p[i].x > p[j].x) && ((i != 0 && j != 5) || p[i].y > p[j].y) && ((i != 1 && j != 4) || p[i].y < p[j].y))) {
          dis.push(dx + dy);
          d[dis[dis.length - 1]] = [i, j];
        }
      }
    }
    if (!dis.length) {
      var res = [0, 4];
    } else {
      res = d[Math.min.apply(Math, dis)];
    }
    var x1 = p[res[0]].x,
      y1 = p[res[0]].y,
      x4 = p[res[1]].x,
      y4 = p[res[1]].y;
    dx = Math.max(Math.abs(x1 - x4) / 2, 10);
    dy = Math.max(Math.abs(y1 - y4) / 2, 10);
    var x2 = [x1, x1, x1 - dx, x1 + dx][res[0]].toFixed(3),
      y2 = [y1 - dy, y1 + dy, y1, y1][res[0]].toFixed(3),
      x3 = [0, 0, 0, 0, x4, x4, x4 - dx, x4 + dx][res[1]].toFixed(3),
      y3 = [0, 0, 0, 0, y1 + dy, y1 - dy, y4, y4][res[1]].toFixed(3);
    var path = ["M", x1.toFixed(3), y1.toFixed(3), "C", x2, y2, x3, y3, x4.toFixed(3), y4.toFixed(3)].join(",");
    if (line && line.line) {
      line.bg && line.bg.attr({path: path});
      line.line.attr({path: path});
    } else {
      var color = typeof line === "string" ? line : COLOR.BLACK;
      return {
        bg: bg && bg.split && this.path(path).attr({
          stroke: bg.split("|")[0],
          fill: "none",
          "stroke-width": bg.split("|")[1] || 3
        }),
        line: this.path(path).attr({
          stroke: color,
          "stroke-width": "2px",
          fill: "none",
          'arrow-end': 'classic-long-wide'
        }),
        from: obj1,
        to: obj2
      };
    }
  };
})(jQuery);
height = $('#main-section').height();
xStart = width = $('#main-section').width();

const dragger = function () {
  // Remove pan from RaphaelZPD object for not crossing drag'n'drop.
  zpd.opts.pan = false;
  // Set coords for main and pair elements.
  [this, this.pair].forEach(value => {
    const isEllipse = value.type === 'ellipse';
    value.ox = isEllipse ? value.attr('cx') : value.attr('x');
    value.oy = isEllipse ? value.attr('cy') : value.attr('y');
    if (value.type !== 'text') value.animate({'fill-opacity': .25}, 500);
  });
};

const move = function (dx, dy) {
  // Move main element and pair element.
  [this, this.pair].forEach(value => {
    const [x, y] = [value.ox + dx, value.oy + dy];
    const isEllipse = value.type === 'ellipse';
    value.attr({[isEllipse ? 'cx' : 'x']: x, [isEllipse ? 'cy' : 'y']: y});
  });

  // Move connections
  for (let i = 0; i < connections.length; i++) {
    paper.connection(connections[i]);
  }
};

const up = function () {
  // Returns pan.
  zpd.opts.pan = true;
  // Fade original and paired element on mouse up.
  [this, this.pair].forEach(value => {
    if (value.type !== 'text') value.animate({'fill-opacity': .25}, 500);
  });
};

function createRectWithText(value) {
  var text = paper
    .text(100, 70 * (texts.length + 1) + 17, value)
    .attr({
      fill: COLOR.BLACK,
      stroke: 'none',
      cursor: 'move',
      'font-weight': 'bold',
      'font-size': 23,
      'font-family': 'URW Chancery'
    });
  var textWidth = text.node.getBBox().width + 10;
  var textHeight = text.node.getBBox().height + 10;
  var rect = paper.rect(100 - textWidth / 2, 70 * (containers.length + 1), textWidth, textHeight).attr({
    fill: COLOR.RED,
    stroke: COLOR.RED,
    "fill-opacity": 0.25,
    "stroke-width": 2,
    cursor: "move",
    hidden: "true"
  }).show("slow");
  var x = containers.length;
  containers.push(rect);
  texts.push(text);
  var tempr = containers[x];
  var tempt = texts[x];
  containers[x].drag(move, dragger, up);
  texts[x].drag(move, dragger, up);
  tempr.pair = tempt;
  tempt.pair = tempr;
  if (!isUndefined(containers[containers.length - 2])) {
    containers[containers.length - 2].attr({
      fill: COLOR.GREY,
      stroke: COLOR.GREY,
      "fill-opacity": 0.5,
      "stroke-width": 2,
      cursor: "move"
    });
    connections.push(paper.connection(containers[containers.length - 2], containers[containers.length - 1], COLOR.BLACK));
  }
  $.growl({title: 'Стек', message: "Элемент со значением '" + value + "' был добавлен", location: "br"});
}

function removeElementFromStack() {
  var value = texts[texts.length - 1].attr('text');
  containers.pop().remove();
  texts.pop().remove();
  for (var i = 0; i < connections.length; i++) {
    connections[i].line.remove();
  }
  for (var x = 0; x < containers.length; x++) {
    if (x > 0) connections.push(paper.connection(containers[x - 1], containers[x], COLOR.BLACK));
  }
  if (!isUndefined(containers[containers.length - 1])) {
    containers[containers.length - 1].attr({
      fill: COLOR.RED,
      stroke: COLOR.RED,
      "fill-opacity": 0.5,
      "stroke-width": 2,
      cursor: "move"
    });
  }
  $.growl({title: 'Стек', message: "Элемент со значением '" + value + "' был удален", location: "br"});
}

function removeElementFromQueue() {
  const value = texts[0].attr('text');
  containers.shift().remove();
  texts.shift().remove();
  for (var i = 0; i < connections.length; i++) {
    connections[i].line.remove();
  }
  for (var x = 0; x < containers.length; x++) {
    if (x > 0) connections.push(paper.connection(containers[x - 1], containers[x], COLOR.BLACK));
  }
  if (!isUndefined(containers[0])) {
    containers[0].attr({fill: COLOR.RED, stroke: COLOR.RED, "fill-opacity": 0.5, "stroke-width": 2, cursor: "move"});
  }
  $.growl({title: 'Очередь', message: "Элемент со значением '" + value + "' был удален", location: "br"});
}

function createQueueElement(value) {
  var text = paper.text(50, 70 * (texts.length + 1) + 13, value).attr({
    fill: COLOR.BLACK,
    stroke: "none",
    "font-weight": "bold",
    "font-size": 15,
    cursor: "move"
  });
  var textWidth = text.node.getBBox().width + 10;
  var textHeight = text.node.getBBox().height + 10;
  var rect = paper.rect(50 - textWidth / 2, 70 * (containers.length + 1), textWidth, textHeight).attr({
    fill: COLOR.GREY,
    stroke: COLOR.GREY,
    "fill-opacity": 0.5,
    "stroke-width": 2,
    cursor: "move"
  });
  var x = containers.length;
  containers.push(rect);
  texts.push(text);
  var tempr = containers[x];
  var tempt = texts[x];
  containers[x].drag(move, dragger, up);
  texts[x].drag(move, dragger, up);
  tempr.pair = tempt;
  tempt.pair = tempr;
  containers[0].attr({fill: COLOR.RED, stroke: COLOR.RED, "fill-opacity": 0.5, "stroke-width": 2, cursor: "move"});
  if (!isUndefined(containers[containers.length - 2])) {
    connections.push(paper.connection(containers[containers.length - 2], containers[containers.length - 1], COLOR.BLACK));
  }
  $.growl({title: 'Очередь', message: "Элемент со значением '" + value + "' был добавлен", location: "br"});
}

function createDequeElement(value, direction) {
  let defaultTextLeft = 50;
  let defaultTextTop = 100 + 70 * (texts.length + 1) + 13;
  let length = containers.length;
  const isEnd = direction === 'end';
  if (length) {
    const t = texts[isEnd ? texts.length - 1 : 0].node.getBBox();
    defaultTextLeft = t.x + t.width / 2;
    defaultTextTop = t.y + 70 * (isEnd ? 1 : -1);
  }
  var text = paper.text(defaultTextLeft, defaultTextTop, value).attr({
    fill: COLOR.BLACK,
    stroke: "none",
    "font-weight": "bold",
    "font-size": 15,
    cursor: "move"
  });
  var textWidth = text.node.getBBox().width + 10;
  var textHeight = text.node.getBBox().height + 10;
  var rect = paper.rect(text.node.getBBox().x - 5, text.node.getBBox().y - 5, textWidth, textHeight).attr({
    fill: COLOR.GREY,
    stroke: COLOR.GREY,
    "fill-opacity": 0.5,
    "stroke-width": 2,
    cursor: "move"
  });

  rect.drag(move, dragger, up);
  text.drag(move, dragger, up);

  const method = direction === 'end' ? 'push' : 'unshift';
  containers[method](rect);
  texts[method](text);

  let tempr = rect, tempt = text;
  Object.assign(tempr, {pair: tempt});
  Object.assign(tempt, {pair: tempr});

  length = containers.length;
  containers[isEnd ? length - 1 : 0].attr({
    fill: isEnd ? COLOR.GREEN : COLOR.RED,
    stroke: isEnd ? COLOR.GREEN : COLOR.RED,
    cursor: 'move',
    'fill-opacity': 0.5,
    'stroke-width': 2
  });
  if (!isUndefined(containers[isEnd ? length - 2 : 1])) {
    containers[isEnd ? length - 2 : 1].attr({
      fill: COLOR.GREY,
      stroke: COLOR.GREY,
      "fill-opacity": 0.5,
      "stroke-width": 2,
      cursor: "move"
    });
    connections.push(paper.connection(containers[isEnd ? length - 2 : 0], containers[isEnd ? length - 1 : 1], COLOR.BLACK));
  }
  if (length === 2) {
    containers[isEnd ? 0 : 1].attr({
      fill: isEnd ? COLOR.RED : COLOR.GREEN,
      stroke: isEnd ? COLOR.RED : COLOR.GREEN,
      cursor: "move",
      "fill-opacity": 0.5,
      "stroke-width": 2
    });
  }
  $.growl({
    title: 'Дек',
    message: `Элемент со значением ${value} был добавлен в ${isEnd ? 'конец' : 'начало'}`,
    location: "br"
  });
  if (containers.length === 1) {
    containers[0].attr({
      fill: COLOR.YELLOW,
      stroke: COLOR.YELLOW,
      "fill-opacity": 0.5,
      "stroke-width": 2,
      cursor: "move"
    });
  }
}

function removeDequeElement(direction) {
  const isEnd = direction === 'end';
  let value = texts[isEnd ? texts.length - 1 : 0].attr('text');
  containers[isEnd ? 'pop' : 'shift']().remove();
  texts[isEnd ? 'pop' : 'shift']().remove();

  for (let i = 0; i < connections.length; i++) {
    connections[i].line.remove();
  }
  for (let x = 0; x < containers.length; x++) {
    if (x > 0) connections.push(paper.connection(containers[x - 1], containers[x], COLOR.BLACK));
  }
  switch (direction) {
    case 'front':
      if (!isUndefined(containers[0])) {
        containers[0].attr({
          fill: COLOR.RED,
          stroke: COLOR.RED,
          "fill-opacity": 0.5,
          "stroke-width": 2,
          cursor: "move"
        });
      }
      $.growl({title: 'Дек', message: "Элемент со значением '" + value + "' был удален с начала", location: "br"});
      break;

    case 'end':
      if (!isUndefined(containers[containers.length - 1])) {
        containers[containers.length - 1].attr({
          fill: COLOR.GREEN,
          stroke: COLOR.GREEN,
          "fill-opacity": 0.5,
          "stroke-width": 2,
          cursor: "move"
        });
      }
      $.growl({title: 'Дек', message: "Элемент со значением '" + value + "' был удален с конца", location: "br"});
      break;
  }
  if (containers.length === 1) {
    containers[0].attr({
      fill: COLOR.YELLOW,
      stroke: COLOR.YELLOW,
      "fill-opacity": 0.5,
      "stroke-width": 2,
      cursor: "move"
    });
  }
}

function drawHashTableChains(size) {
  for (var i = 0; i < size; i++) {
    containers[i] = [];
    texts[i] = [];
    var rect = paper.rect(50, 50 * (i + 1), 50, 50).attr({stroke: COLOR.RED, fill: COLOR.RED, "fill-opacity": 0.25});
    containers[i].push(rect);
  }
}

function drawHashTableOpenAddresses(size) {
  for (var i = 0; i < size; i++) {
    var rect = paper.rect(50, 50 * (i + 1), 50, 50).attr({
      fill: COLOR.RED,
      "fill-opacity": .25,
      stroke: COLOR.RED,
      "stroke-width": "2px"
    });
    containers.push(rect);
  }
}

function initHashTableChains(value) {
  drawHashTableChains(value);
  containers.forEach(function (vvv, iii, ccc) {
    var ids = ['#first_text-' + iii, '#first_rect-' + iii];
    var contexts = [texts[iii], containers[iii]];
    ids.forEach((value, ind) => {
      var context = contexts[ind];
      $('#main-section').on('click', ids[ind], function (e) {
        var c = 0;
        for (var xj = 1; xj < context.length; xj++) {
          if (context[xj].isVisible()) {
            c++;
            break;
          }
        }
        context.forEach((vv, ii) => {
          if (ii > 0) {
            containers[iii][ii][c ? 'hide' : 'show']('slow');
            texts[iii][ii][c ? 'hide' : 'show']('slow');
          }
        });
      })
    });
  });
}

function createHashTableChainsElement(value) {
  var index = hash(value) % containers.length;
  var block = containers[index];
  if (texts[index].length) {
    var lastRect = containers[index][containers[index].length - 1];
    var rect = paper.rect(lastRect.attr('x') + lastRect.attr('width'), lastRect.attr('y'), 50, lastRect.attr('height')).attr({stroke: COLOR.RED});
    block.push(rect);
    containers[index][0].node.id = 'first_rect-' + index;
    var text = paper.text(-100, -100, value).attr({
      fill: COLOR.BLACK,
      stroke: 'none',
      'font-weight': 'bold',
      'font-size': 15
    });
    text.node.id = 'first_text-' + index;
    texts[index].push(text);
    block[0].attr({cursor: 'pointer'});
    block[0].attr({'fill-opacity': 0.5});
    texts[index][0].attr({cursor: 'pointer'});
    containers[index][containers[index].length - 1].hide();
    texts[index][texts[index].length - 1].hide();
    var m = 0;
    for (var xj = 1; xj < containers[index].length; xj++) {
      if (containers[index][xj].isVisible()) {
        m++;
        break;
      }
    }
    containers[index][containers[index].length - 1].show();
    texts[index][texts[index].length - 1].show();
    var rectBlock = block[block.length - 1].node.getBBox();
    var height = rectBlock.height;
    var width = rectBlock.width;
    var y = rectBlock.y;
    var bBox = text.node.getBBox();
    var offset = 0;
    var textWidth = bBox.width;
    if (textWidth + 10 > width) {
      block[block.length - 1].attr({width: bBox.width + 20});
      texts[index][block.length - 1].attr({
        x: rectBlock.x + bBox.width / 2 + 10,
        y: rectBlock.y + rectBlock.height / 2
      });
    } else {
      texts[index][block.length - 1].attr({
        x: rectBlock.x + rectBlock.width / 2,
        y: rectBlock.y + rectBlock.height / 2
      });
    }
    if (!m) {
      containers[index][containers[index].length - 1].hide();
      texts[index][texts[index].length - 1].hide();
    }
  } else {
    containers[index][0].node.id = 'first_rect-' + index;
    rectBlock = block[0].node.getBBox();
    height = rectBlock.height;
    width = rectBlock.width;
    y = rectBlock.y;
    text = paper.text(-100, -100, value).attr({
      fill: COLOR.BLACK,
      stroke: 'none',
      'font-weight': 'bold',
      'font-size': 15
    });
    text.node.id = 'first_text-' + index;
    bBox = text.node.getBBox();
    offset = 0;
    textWidth = bBox.width;
    if (textWidth + 10 > width) {
      for (var mm = 0; mm < containers.length; mm++) {
        containers[mm][0].attr({width: bBox.width + 20});
        if (containers[mm].length) {
          for (xx = 1; xx < containers[mm].length; xx++) {
            var currentRectX = containers[mm][xx].attr('x');
            var currentTextX = texts[mm][xx].attr('x');
            var delta = containers[mm][0].attr('width') - width;
            containers[mm][xx].attr({x: currentRectX + delta});
            texts[mm][xx].attr({x: currentTextX + delta});
          }
        }
        if (!isUndefined(texts[mm][0])) {
          tmp = containers[mm][0].node.getBBox();
          tmp1 = texts[mm][0].node.getBBox();
          texts[mm][0].attr({x: tmp.x + tmp.width / 2, y: tmp.y + tmp.height / 2});
        }
      }
      text.attr({x: rectBlock.x + bBox.width / 2 + 10, y: rectBlock.y + rectBlock.height / 2});
    } else {
      text.attr({x: rectBlock.x + rectBlock.width / 2, y: rectBlock.y + rectBlock.height / 2});
    }
    texts[index].push(text);
  }
}

function removeHashTableChainsElement(value) {
  var nonempty = 0;
  texts.forEach(value => {
    if (value.length) {
      nonempty++;
      return false;
    }
  });
  if (!nonempty) {
    $.growl.error({title: 'Ошибка!', message: 'Хеш-таблица пуста', location: 'br'});
  } else {
    var index = hash(value) % containers.length;
    var block = containers[index];
    var text = texts[index];
    text.some(function (val, ind, cb) {
      if (val.attr('text') === value) {
        var isHidden = !block[ind].isVisible();
        block[ind].show();
        var width = block[ind].node.getBBox().width;
        if (isHidden) {
          block[ind].hide();
        }
        if (!ind) {
          for (var xk = 1; xk < containers[index].length; xk++) {
            var tmpx = containers[index][xk].attr('x');
            var tmptx = texts[index][xk].attr('x');
            containers[index][xk].attr({x: tmpx - width});
            texts[index][xk].attr({x: tmptx - width});
          }
          texts[index][ind].remove();
          texts[index].shift();
          if (!isUndefined(texts[index][0])) {
            texts[index][0].node.id = 'first_text-' + index;
          }
          if (containers[index].length > 1) {
            containers[index][ind].remove();
            containers[index].shift();
            if (containers[index].length > 1) {
              containers[index][0].node.id = 'first_rect-' + index;
              containers[index][0].attr({fill: COLOR.RED, 'fill-opacity': .5});
            } else {
              containers[index][0].attr({fill: COLOR.RED, 'fill-opacity': .25});
            }
            if (!containers[index][0].isVisible()) {
              containers[index][0].show();
              texts[index][0].show();
            }
          }
        } else {
          for (xk = ind + 1; xk < containers[index].length; xk++) {
            tmpx = containers[index][xk].attr('x');
            tmptx = texts[index][xk].attr('x');
            containers[index][xk].attr({x: tmpx - width});
            texts[index][xk].attr({x: tmptx - width});
          }
          texts[index][ind].remove();
          texts[index].splice(ind, 1);
          containers[index][ind].remove();
          containers[index].splice(ind, 1);
        }
        return true;
      }
    });
  }
}

function binaryTreeCreateElement(value) {
  const val = parseInt(value);
  if (typeof tree === 'undefined') {
    tree = new BinaryTree(val);
  } else {
    tree.addNode(val);
  }
  texts.push(val);
  const getXCoordinate = function (t, x, deviation = 50) {
    if (t.value === val) {
      return x;
    } else {
      x += val < t.value ? -deviation : deviation;
      return getXCoordinate((val < t.value ? t.leftNode : t.rightNode), x, deviation * 0.65);
    }
  };

  const getYCoordinate = function (t, y) {
    if (t.value === val) {
      return y;
    } else {
      y += 50;
      return getYCoordinate((val < t.value ? t.leftNode : t.rightNode), y)
    }
  };
  var text = paper
    .text(getXCoordinate(tree, 200), getYCoordinate(tree, 70), val)
    .attr({
      fill: COLOR.BLACK,
      stroke: 'none',
      'font-weight': 'bold',
      'font-size': 23,
      'font-family': 'URW Chancery'
    });
  var bbox = text.node.getBBox();
  var textWidth = bbox.width;
  var textHeight = bbox.height;
  var x = bbox.x;
  var y = bbox.y;
  var rect = paper.rect(text.node.getBBox().x - 5, text.node.getBBox().y - (textWidth + 10 - bbox.height) / 2, textWidth + 10, textWidth + 10).attr({
    fill: COLOR.RED,
    stroke: COLOR.RED,
    'fill-opacity': 0.25,
    'stroke-width': 2,
    hidden: 'true',
    text: text
  }).show('slow');
  containers.push(rect);
  x = containers.length - 1;
  var tempr = containers[x];
  var tempt = texts[x];
  // @todo: Refactor this one. Looks like it'll not performance.
  // @todo: Also check line draw algo.
  const bindSquares = function (tree, value, parent) {
    if (tree.value === value) {
      return parent;
    } else {
      if (tree.leftNode) {
        let parent = bindSquares(tree.leftNode, value, tree);
        if (parent) {
          let parentIndex = texts.indexOf(parent.value);
          let childIndex = texts.indexOf(value);
          connections.push(paper.connection(containers[parentIndex], containers[childIndex], COLOR.BLACK));
        }
      }
      if (tree.rightNode) {
        let parent = bindSquares(tree.rightNode, value, tree);
        if (parent) {
          let parentIndex = texts.indexOf(parent.value);
          let childIndex = texts.indexOf(value);
          connections.push(paper.connection(containers[parentIndex], containers[childIndex], COLOR.BLACK));
        }
      }
    }
  };
  bindSquares(tree, val, tree);
  if (!isUndefined(containers[containers.length - 2])) {
    // containers[containers.length - 2].attr({fill: COLOR.GREY, stroke: COLOR.GREY, 'fill-opacity': 0.5, 'stroke-width': 2, cursor: 'move'});
    // connections.push(paper.connection(containers[containers.length - 2], containers[containers.length - 1], COLOR.BLACK));
  }
  $.growl({
    title: 'Двоичное дерево поиска',
    message: "Элемент со значением '" + value + "' был добавлен",
    location: "br"
  });
}

function binaryTreeRemoveElement(value) {

}

function initHashTableOpenAddresses(size) {
  isDeleted = [];
  for (var j = 0; j < size; j++) isDeleted[j] = false;
  drawHashTableOpenAddresses(size);
}

function createHashTableOpenAddressesElement(value) {
  var index = hash(value) % containers.length;
  var textCount = 0;
  for (var j = 0; j < containers.length; j++) {
    if (!isUndefined(texts[j]) && !isDeleted[j]) textCount++;
  }
  if (textCount !== containers.length) {
    var firstContainer = containers[0].node.getBBox();
    var text = paper.text(firstContainer.x + 70 + firstContainer.width / 2, firstContainer.y + firstContainer.height / 2, value).attr({
      fill: COLOR.BLACK,
      stroke: 'none',
      'font-weight': 'bold',
      'font-size': 15
    });
    var tempRect = paper.rect(firstContainer.x + 70, firstContainer.y, firstContainer.width, firstContainer.height).attr({text: text});
    if (isUndefined(texts[index]) || isDeleted[index]) {
      var diffY = firstContainer.y + firstContainer.width / 2 + index * firstContainer.height - firstContainer.y;
      var diffX = 70;
      tempRect.animate({
        y: firstContainer.y + index * firstContainer.height
      }, diffY * 5);
      text.animate({
        y: firstContainer.y + firstContainer.height / 2 + index * firstContainer.height
      }, diffY * 5);
      setTimeout(function () {
        tempRect.animate({
          x: firstContainer.x
        }, diffX * 5);
        text.animate({
          x: firstContainer.x + firstContainer.width / 2
        }, diffX * 5, function () {
          containers[index].attr({fill: COLOR.GREEN});
        });
      }, diffY * 5);
      if (isDeleted[index]) {
        texts[index].remove();
      }
      texts[index] = text;
      isDeleted[index] = false;
    } else {
      var position = index;
      var nextIndex = index + 1;
      if (nextIndex > containers.length - 1) {
        nextIndex = 0;
      }
      diffY = containers[nextIndex].attr('y');
      diffX = 70;
      while (!isUndefined(texts[nextIndex]) && nextIndex !== position && !isDeleted[nextIndex]) {
        nextIndex++;
        if (nextIndex > containers.length - 1) {
          nextIndex = 0;
        }
      }
      if (nextIndex !== position) {
        diffY = containers[nextIndex].attr('y');
        tempRect.animate({
          y: diffY
        }, diffY * 5);
        text.animate({
          y: diffY + firstContainer.height / 2
        }, diffY * 5);
        setTimeout(function () {
          tempRect.animate({
            x: firstContainer.x
          }, diffX * 5);
          text.animate({
            x: firstContainer.x + firstContainer.width / 2
          }, diffX * 5, function () {
            containers[nextIndex].attr({fill: COLOR.GREEN});
          });
        }, diffY * 5);
        if (isDeleted[nextIndex]) {
          texts[nextIndex].remove();
        }
        texts[nextIndex] = text;
        isDeleted[nextIndex] = false;
      }
    }
  } else {
    $.growl.error({message: 'Хеш-таблица таблица полностью заполнена!', title: 'Ошибка', location: 'br'});
  }
}

function removeHashTableOpenAddressesElement(value) {
  var index = hash(value) % containers.length;
  var block = containers[index].node.getBBox();
  if (value === texts[index].attr('text') && !isDeleted[index]) {
    var r = paper.rect(block.x, block.y, block.width, block.height).attr({fill: COLOR.BLACK, 'fill-opacity': 0.25});
    containers[index].animate({
      fill: COLOR.BLACK,
      'fill-opacity': 0.25,
      stroke: COLOR.BLACK
    }, 200, function () {
      r.remove();
    });
    isDeleted[index] = true;
  } else {
    var position = index, nextIndex = index + 1;
    if (nextIndex > containers.length - 1) {
      nextIndex = 0;
    }
    while (nextIndex !== position
    && !isUndefined(texts[nextIndex])
    && (texts[nextIndex].attr('text') !== value || (texts[nextIndex].attr('text') === value && isDeleted[nextIndex]))) {
      nextIndex++;
      if (nextIndex > containers.length - 1) nextIndex = 0;
    }
    if (nextIndex !== position) {
      if (isUndefined(texts[nextIndex])) {
        $.growl.error({title: 'Удаление', message: 'Такой элемент не содержится в хэш-таблице'});
      } else {
        if (index < nextIndex) {
          r = paper.rect(block.x, block.y, block.width, block.height).attr({
            fill: COLOR.BLACK,
            'fill-opacity': 0.25,
            stroke: COLOR.BLACK
          });
          for (i = index; i < nextIndex; i++) {
            block = containers[i + 1].node.getBBox();
            r.animate({
              y: block.y
            }, 1000, function () {
              containers[i].animate({
                fill: COLOR.BLACK,
                'fill-opacity': 0.25,
                stroke: COLOR.BLACK
              }, 200, function () {
                r.remove();
              });
            });
          }
          isDeleted[nextIndex] = true;
        } else {
          r = paper.rect(block.x, block.y, block.width, block.height).attr({
            fill: COLOR.BLACK,
            'fill-opacity': 0.25,
            stroke: COLOR.BLACK
          });
          if (nextIndex) {
            for (var i = 0; i < nextIndex; i++) {
              r.attr({y: containers[0].attr('y')});
              block = containers[i + 1].node.getBBox();
              r.animate({
                y: block.y
              }, 1000, function () {
                containers[i].animate({
                  fill: COLOR.BLACK,
                  'fill-opacity': 0.25,
                  stroke: COLOR.BLACK
                }, 200, function () {
                  r.remove();
                });
              });
            }
          } else {
            containers[nextIndex].animate({
              fill: COLOR.BLACK,
              'fill-opacity': 0.25,
              stroke: COLOR.BLACK
            }, 200, function () {
              r.remove();
            });
          }
          isDeleted[nextIndex] = true;
        }
      }
    } else {
      if (isUndefined(texts[nextIndex])) {
        $.growl.error({title: 'Удаление', message: 'Такой элемент не содержится в хэш-таблице'});
      }
    }
  }
}

function heapMaxCreateElement(value) {
  var value = parseInt(value);
  if (!texts.length) {
    containers[0] = paper.rect(50, 50, 50, 50).attr({
      fill: COLOR.RED,
      "fill-opacity": .25,
      stroke: COLOR.RED,
      "stroke-width": "2px"
    });
    var bBox = containers[0].node.getBBox();
    texts[0] = paper.text(bBox.x + bBox.width / 2, bBox.y + bBox.height / 2, value).attr({
      "font-family": 'URW Chancery',
      "font": 'bold',
      "font-size": 15
    });
    heapTree = {
      value: texts[0],
      container: containers[0],
      left: {},
      right: {}
    };
  } else {
    containers[texts.length] = paper.rect(50, 50 * (texts.length + 1), 50, 50);
    var bbox = containers[texts.length].node.getBBox();
    texts[texts.length] = paper.text(bbox.x + bbox.width / 2, bbox.y + bbox.height / 2, value);
    var currentPosition = texts.length - 1, swapped = -1;
    while (currentPosition > 0 && parseInt(texts[currentPosition].attr('text')) > parseInt(texts[parseInt((currentPosition - 1) / 2)].attr('text'))) {
      swapped = currentPosition;
      var tmp = texts[currentPosition];
      texts[currentPosition] = texts[parseInt((currentPosition - 1) / 2)];
      texts[parseInt((currentPosition - 1) / 2)] = tmp;
      currentPosition = parseInt((currentPosition - 1) / 2);
    }
    if (swapped !== -1) {
      texts[parseInt((swapped - 1) / 2)].animate({
        x: containers[parseInt((swapped - 1) / 2)].attr('x') - 30
      }, 200, function () {
        texts[parseInt((swapped - 1) / 2)].animate({
          y: containers[parseInt((swapped - 1) / 2)].attr('y') + containers[parseInt((swapped - 1) / 2)].attr('height') / 2
        }, 200, function () {
          texts[parseInt((swapped - 1) / 2)].animate({
            x: containers[parseInt((swapped - 1) / 2)].attr('x') + containers[parseInt((swapped - 1) / 2)].attr('width') / 2
          }, 200);
        });
      });
      for (var m = swapped; m < texts.length; m++) {
        texts[m].animate({
          y: containers[m].attr('y') + containers[m].attr('height') / 2
        }, 200);
      }
    }
    heapTree = {};
    let heapValues = $.extend([], texts);
    let heaps = [];
    while (heapValues.length !== 1) {
      const x = heapValues.length - 1, leafIndex = parseInt((x - 1) / 2);
      if (isUndefined(heaps[leafIndex])) heaps[leafIndex] = {};
      heaps[leafIndex].value = heapValues[leafIndex];
      heaps[leafIndex][!isEven(x) ? 'left' : 'right'] = heapValues[x];
      heapValues.pop();
    }
    while (heaps.length !== 1) {
      const x = heaps.length - 1, leafIndex = parseInt((x - 1) / 2);
      heaps[leafIndex][!isEven(x) ? 'left' : 'right'] = heaps[x];
      heaps.pop();
    }
    if (!isUndefined(window.treeSet)) {
      treeSet.remove();
      connections = [];
    }
    treeSet = paper.set();
    var coords = generateCoordinates(heaps[0], undefined, undefined, undefined);
    for (var i = 0; i < coords.length; i++) {
      if (!isUndefined(coords[i].parent)) {
        connections.push(paper.connection(coords[i].parent, treeSet.items[i], COLOR.BLACK));
      }
    }
  }
}

function generateCoordinates(tree, value, dir, parent) {
  let coordinates = [];
  if (!isUndefined(tree)) {
    let tmp, texts;
    if (isUndefined(value)) {
      texts = paper.text(400, 75, tree.value.attr('text'));
      treeSet.push(texts);
      tmp = new Point(400, 75, tree.value.attr('text'), 1, undefined);
    } else {
      let x = dir === 'left' ? value.x - 300 / Math.pow(2, value.depth) : value.x + 300 / Math.pow(2, value.depth);
      let y = value.y + 40;
      let text = !isUndefined(tree.value) ? tree.value.attr('text') : tree.attr('text');
      texts = paper.text(x, y, text);
      treeSet.push(texts);
      tmp = new Point(x, y, text, value.depth + 1, parent);
    }
    coordinates.push(tmp);
    ['left', 'right'].forEach(direction => {
      if (!isUndefined(tree[direction])) $.merge(coordinates, generateCoordinates(tree[direction], tmp, direction, texts));
    });

    return coordinates;
  }
  return coordinates;
}

function init(structure) {
  getFileContent('/misc/structure_list.json')
    .then(
      structureList => {
        const structureInfo = structureList.find(item => item.machine_name === structure);
        const $mainSection = $('#main-section');
        $mainSection.empty();
        if (!isUndefined(window.paper)) paper.remove();
        containers = [];
        texts = [];
        connections = [];
        const $rightSection = $('#right-section');
        getFileContent(`/misc/descriptions/${structure}.html`)
          .then(result => $rightSection.html(result));
        $mainSection.append('<div class="group-wrapper"><div id="standard-group" class="input-group"><span class="input-group-btn">'
          + '<button id="add_element" class="btn btn-default" type="button">Добавить элемент</button></span>'
          + '<input id="element" type="text" class="form-control" placeholder="Значение элемента">' +
          '<span class="input-group-btn"><button id="remove-element" class="btn btn-default" type="button">Удалить элемент</button></span></div></div>');
        let mainSectionOffset = $('#main-section').offset();
        paper = new Raphael(mainSectionOffset.left, mainSectionOffset.top, $('#main-section').width(), $('#main-section').height() -  ($(window).height() - $('.group-wrapper').height() + 50));
        zpd = new RaphaelZPD(paper, {zoom: true, pan: true, drag: false});
        $(document).attr('title', structureInfo.name + ' - Визуализация структур данных');
        $('#intro_text').remove();
        var remove = document.createElement('input');
        remove.type = 'button';
        remove.value = 'Remove element';
        remove.className = 'form-control';
        $('#add_element').click((e) => {
          const value = $('#element').val();
          if (value) {
            switch (structure) {
              case 'Stack':
                createRectWithText(value);
                break;

              case 'Queue':
                createQueueElement(value);
                break;

              case 'Deque':
                createDequeElement(value, 'end');
                break;

              case 'HashTableChains':
                createHashTableChainsElement(value);
                break;

              case 'BinaryTree':
                if (isInt(value)) binaryTreeCreateElement(value);
                break;

              case 'HashTableOpenAddresses':
                createHashTableOpenAddressesElement(value);
                break;

              case 'HeapMax':
                if (isInt(value)) heapMaxCreateElement(value);
                break;
            }
          }
        });
        $('#remove-element').click((e) => {
          if (texts.length) {
            const value = $('input#element').val();
            switch (structure) {
              case 'Stack':
                removeElementFromStack();
                break;

              case 'Queue':
                removeElementFromQueue();
                break;

              case 'Deque':
                removeDequeElement('end');
                break;

              case 'HashTableChains':
                if (value) removeHashTableChainsElement(value);
                break;

              case 'BinaryTree':
                if (value) binaryTreeRemoveElement(value);
                break;

              case 'HashTableOpenAddresses':
                if (value) removeHashTableOpenAddressesElement(value);
                break;

              case 'HeapMax':
                if (value && isInt(value)) {
                  heapMaxRemoveElement(value);
                }
                break;
            }
          }
        });
        if (structure === 'Deque') {
          $('.group-wrapper').append('<br><div class="input-group"><span class="input-group-btn">'
            + '<button id="add-element-front" class="btn btn-default" type="button">Добавить элемент в начало</button></span>'
            + '<input id="element-front" type="text" class="form-control" placeholder="Значение элемента">' +
            '<span class="input-group-btn"><button id="remove-element-front" class="btn btn-default" type="button">Удалить элемент с начала</button></span></div>')
          $('#remove-element-front').click((e) => {
            if (texts.length) removeDequeElement('front');
          });
          $('#add-element-front').click((e) => {
            const value = $('#element-front').val();
            if (value) createDequeElement(value, 'front');
          });
        }
        if ('HashTableChains' === structure || 'HashTableOpenAddresses' === structure) {
          // Hide until create
          $('#standard-group').hide();
          $mainSection.append('<div class="group-wrapper"><div id="hashtablechains" class="input-group">'
            + '<span class="input-group-btn">'
            + '<button id="hashtablechains-create" class="btn btn-default" type="button">Создать хеш-таблицу</button>'
            + '</span>'
            + '<input id="hashtablechains-quantity" type="text" class="form-control" placeholder="Количество элементов"></div></div>');
          $('#hashtablechains-create').click((e) => {
            const quantity = $('#hashtablechains-quantity').val();
            if (isInt(quantity)) {
              $('#hashtablechains').hide();
              $('#standard-group').show();
              if (structure === 'HashTableChains') {
                initHashTableChains(quantity);
              } else {
                initHashTableOpenAddresses(quantity);
              }
            } else {
              $.growl.error({title: 'Ошибка!', message: 'Введите целое число для размера хеш-таблицы', location: 'br'})
            }
          });
        }
      }
    );
}
