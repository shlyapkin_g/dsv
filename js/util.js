/**
 * Check if passed argument is integer.
 *
 * @param {*} value - Argument for check.
 */
const isInt = value => parseFloat(value) % 1 === 0;

/**
 * Check if passed argument is undefined.
 *
 * @param {*} value - Argument for check.
 */
const isUndefined = value => typeof value === 'undefined';

/**
 * Check if passed argument is even.
 *
 * @param {*} value - Argument for check.
 */
const isEven = value => value % 2;

/**
 * Returns Promise which fetching structure list.
 * @return {Promise} - Promise.
 */
const getFileContent = (path) => {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: path,
            method: 'GET',
            success: data => resolve(data),
            error: err => resolve('')
        });
    })
};
