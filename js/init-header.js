'use strict';
($ => {
    getFileContent('/misc/structure_list.json')
        .then(result => {
            const $ul = $('ul#data-structure-vizualizers');
            result.sort((a, b) => {
                if (a.weight === b.weight) return 0;
                return a.weight < b.weight ? -1 : 1;
            }).forEach(value => {
                const $li = $('<li/>').appendTo($ul).click(e => {
                    tree = undefined;
                    init(value['machine_name']);
                });
                const a = $("<a/>")
                    .text(value['name'])
                    .attr('href', '#')
                    .appendTo($li);
            });
        }).catch(console.log);
})(jQuery);