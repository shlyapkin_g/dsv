const express = require('express');

// Init express.
const app = express();
app.listen(3000);

// Set default view engine.
app.set('view engine', 'pug');
app.set('views', __dirname + '/views');

// Handle static files.
app.use('/css', express.static('css'));
app.use('/libraries', express.static('libraries'));
app.use('/fonts', express.static('fonts'));
app.use('/js', express.static('js'));
app.use('/misc', express.static('misc'));

// Define the only route.
app.get('/', (req, res) => res.render('index'));

// Handle 404.
app.use((req, res, next) => {
  res.status(404).render('404');
});
